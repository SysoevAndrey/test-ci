describe('app spec', () => {
  it('render the list of followers', () => {
    cy.visit('http://localhost:5173')

    cy.get('input').type('some text');
    cy.get('button').click();

    cy.get('.list').children().should('have.length', 4);
    cy.get('.list').children().last().children().first().contains('✅');
  })
})