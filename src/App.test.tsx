import { fireEvent, render, screen } from '@testing-library/react';
import App from './App';

describe('App component', () => {
    it('should fill form and submit', () => {
        render(<App />);

        const input: HTMLInputElement = screen.getByTestId('input');
        expect(input).toBeInTheDocument();

        const button: HTMLButtonElement = screen.getByText('Submit');
        expect(button).toBeDisabled();

        fireEvent.change(input, { target: { value: 'some text...' } });
        expect(input.value).toBe('some text...');
        expect(button).toBeEnabled();
    });
});
