import { ChangeEvent, FormEvent, useCallback, useState } from 'react';
import './App.css';
import { IUser } from './types/user';
import { validateInput } from './utils/validateInput';
import { enrichWithStatus } from './utils/enrichWithStatus';

const USERS_MOCK = [
    { name: 'Bob', followers: ['1', '5', '10', '22'] },
    { name: 'Mike', followers: ['1', '7', '12', '18', '21'] },
    { name: 'undefined', followers: [] },
    { name: 'Anna', followers: ['2', '3', '8', '17', '26', '28'] },
];

function App() {
    const [value, setValue] = useState('');
    const [disabled, setDisabled] = useState(true);
    const [users, setUsers] = useState<IUser[]>([]);

    const onSubmit = useCallback((event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
    }, []);

    const onChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);

        const isValid = validateInput(event.target.value);
        setDisabled(!isValid);
    }, []);

    const onClick = useCallback(() => {
        const enrichedUsers = enrichWithStatus(USERS_MOCK);
        setUsers(enrichedUsers);
    }, []);

    return (
        <form onSubmit={onSubmit}>
            <input data-testid="input" value={value} onChange={onChange} />
            <button disabled={disabled} onClick={onClick}>
                Submit
            </button>
            <div className="list">
                {users.map((user) => (
                    <div>
                        <h3>
                            {user.name}&nbsp;
                            {user.isPopular && <span>✅</span>}
                        </h3>
                        <p>Followers: {user.followers.length}</p>
                    </div>
                ))}
            </div>
        </form>
    );
}

export default App;
