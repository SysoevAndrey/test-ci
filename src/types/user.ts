export interface IUser {
    name: string;
    followers: string[];
    isPopular: boolean;
}
