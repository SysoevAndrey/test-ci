import { enrichWithStatus } from '../enrichWithStatus';

describe('enrichWithStatus', () => {
    it('should add isPopular property', () => {
        const data = [
            { name: 'Bob', followers: ['1', '5', '10', '22'] },
            { name: 'Mike', followers: ['1', '7', '12', '18', '21'] },
            { name: 'undefined', followers: [] },
            { name: 'Anna', followers: ['2', '3', '8', '17', '26', '28'] },
        ];

        const expectedResult = [
            {
                name: 'Bob',
                followers: ['1', '5', '10', '22'],
                isPopular: false,
            },
            {
                name: 'Mike',
                followers: ['1', '7', '12', '18', '21'],
                isPopular: false,
            },
            { name: 'undefined', followers: [], isPopular: false },
            {
                name: 'Anna',
                followers: ['2', '3', '8', '17', '26', '28'],
                isPopular: true,
            },
        ];

        const result = enrichWithStatus(data);

        expect(result).toEqual(expectedResult);
    });
});
