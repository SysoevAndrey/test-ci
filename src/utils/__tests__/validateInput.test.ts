import { validateInput } from '../validateInput';

describe('validateInput', () => {
    it('should return false for too short', () => {
        expect(validateInput('test')).toBe(false);
    });

    it('should return false for too long', () => {
        expect(
            validateInput('Some long text that has more than 25 symbols')
        ).toBe(false);
    });

    it('should return true for normal length', () => {
        expect(validateInput('hello world')).toBe(true);
    });
});
