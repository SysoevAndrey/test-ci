export const enrichWithStatus = (
    users: { name: string; followers: string[] }[]
): { name: string; followers: string[]; isPopular: boolean }[] =>
    users.map((user) => ({
        ...user,
        isPopular: user.followers.length > 5,
    }));
