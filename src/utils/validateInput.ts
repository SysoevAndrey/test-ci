export const validateInput = (input: string): boolean => {
    if (input.length < 5) {
        return false;
    }

    if (input.length > 25) {
        return false;
    }

    return true;
};
